/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
} from 'react-native';

import {
  Container,
  StyleProvider,
} from 'native-base';

import getTheme from './src/theme/components';
import platform from './src/theme/variables/platform';

import AppBody from './src/components/AppBody';
import AppHeader from './src/components/AppHeader';
import AppFooter from './src/components/AppFooter';

export default class estudandoNative extends Component {
  render() {
    return (
      <StyleProvider style={getTheme(platform)}>
        <Container>
          <AppHeader />
          <AppBody />
          <AppFooter />
        </Container>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

AppRegistry.registerComponent('estudandoNative', () => estudandoNative);
