/*
  Geo Icon
*/

import React, { Component } from 'react';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../../selection.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class Geo extends Component {
  render() {
    return (
      <Icon name="geografia" color="#4F8EF7"/>
    );
  }
}
