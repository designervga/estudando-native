/*
  Lin Icon
*/

import React, { Component } from 'react';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../../selection.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class Lin extends Component {
  render() {
    return (
      <Icon name="linguas" color="#4F8EF7"/>
    );
  }
}
