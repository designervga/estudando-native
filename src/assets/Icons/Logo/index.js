/*
  Logo Icon
*/

import React, { Component } from 'react';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../../selection.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class Logo extends Component {
  render() {
    return (
      <Icon name="guia" color="#4F8EF7"/>
    );
  }
}
