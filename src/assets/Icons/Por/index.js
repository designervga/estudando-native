/*
  Por Icon
*/

import React, { Component } from 'react';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../../selection.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class Por extends Component {
  render() {
    return (
      <Icon name="portugues" color="#4F8EF7"/>
    );
  }
}
