/*
  Subject Icon - Como fazer isso com props? Ideia é a materia entrar como name.
*/

import React, { Component } from 'react';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../../selection.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class SubjectIcon extends Component {
  render() {
    return (
      <Icon name="this.props.title" color="#4F8EF7"/>
    );
  }
}
