/* AppBody */

import React, { Component } from 'react';
import styles from "./styles";

import {
  StyleSheet,
} from 'react-native';

import {
  Content,
  Body,
} from 'native-base';

import DisciplinesCollapsible from '../DisciplinesCollapsible';

export default class appBody extends Component {
  render() {
    return (
      <Content style={styles.container}>
        <Body>
          <DisciplinesCollapsible />
        </Body>
      </Content>
    );
  }
}
