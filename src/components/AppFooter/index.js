/* AppFooter */

import React, { Component } from 'react';
import {
  StyleSheet,
} from 'react-native';

import {
  Footer,
  Button,
  Text,
  FooterTab,
} from 'native-base';

import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../selection.json';
import Ionicon from 'react-native-vector-icons/Ionicons';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class appFooter extends Component {
  render() {
    return (
      <Footer>
        <FooterTab>
          <Button vertical>
            <Icon name="guia" size={20} />
            {/* <Text style={{fontSize: 10}}></Text> */}
          </Button>
          <Button vertical>
            <Icon name="ao-vivo" size={24} />
            {/* <Text style={{fontSize: 10}}></Text> */}
          </Button>
          <Button vertical active >
            <Icon active name="disciplinas" size={20} />
            {/* <Text style={{fontSize: 10}}></Text> */}
          </Button>
          <Button vertical>
            <Ionicon name="md-person" size={22} />
            {/* <Text style={{fontSize: 10}}></Text> */}
          </Button>
        </FooterTab>
      </Footer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
