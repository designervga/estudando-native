/*
  CheckoutBoletoThankYou
*/

import React, { Component } from 'react';
import styles from "./styles";
import { Grid, Row, Col } from 'react-native-easy-grid';
import {
  StyleSheet,
  View,
} from 'react-native';

import {
  Text,
  Content,
  Button,
} from 'native-base';

export default class CheckoutBoletoThankYou extends Component {
  render() {
    return (
      <Grid style={[ styles.contentWrapper, { minHeight: 500 }]}>
        {/* Valmir: Fix parent flex cascade when screen is coded. Remove fixed height. */}
        <Row size={3} style={styles.upperRow}>
          <Col>
            <Text  style={styles.upperRowMessageText}>
              Uma cópia do boleto foi gerada e enviada para seu email carloshenrique@gmail.com.
            </Text>
            <Button block style={styles.barcodeButton}>
              <Text>
                Ver código
              </Text>
            </Button>
            <Button block style={styles.saveButton}>
              <Text>
                Salvar boleto
              </Text>
            </Button>
          </Col>
        </Row>
        <Row size={1}>
          <Col>
            <Button block light style={styles.buttonTertiary}>
              <Text>
                Ir para o guia de estudos
              </Text>
            </Button>
          </Col>
        </Row>
      </Grid>
    );
  }
}
