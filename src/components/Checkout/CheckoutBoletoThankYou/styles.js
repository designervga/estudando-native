/*
  CheckoutBoletoThankYou styles
 */

import React, { Component } from 'react';

const { StyleSheet } = React;
import variables from "../../../theme/variables/platform";

export default {
  contentWrapper: {
    paddingHorizontal: variables.screenMarginHor,
  },
  upperRow: {
    marginTop: 24,
  },
  upperRowMessageText: {
    marginBottom: 12,
  },
  lowerRow: {
    height: 50,
  },
  barcodeButton: {
    marginTop: 12,
  },
  saveButton: {
    marginTop: 12,
  },
};
