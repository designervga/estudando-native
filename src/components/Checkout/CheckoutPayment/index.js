/*
  CheckoutPayment
*/

import React, { Component } from 'react';
import styles from "./styles";
import { Grid, Row, Col } from 'react-native-easy-grid';

import {
  View,
} from 'react-native';

import {
  Text,
  Button,
  Content,
  Form,
  Item,
  Picker,
  PickerItem as FormItem,
  Input,
  Icon,
  Label,
  CheckBox,
  Body,
  ListItem,
  H3,
} from 'native-base';

// Payment methods
const PickerItem = Picker.Item;
const creditCardRecurrent = "Cartão de crédito"
const creditCardInstallment = "Cartão de crédito (12x de 17,90)"
const creditCardOnePayment = "Cartão de crédito (À vista - 190,00)"
const boletoOnePayment = "Boleto (À vista - 190,00)"

export default class CheckoutPayment extends Component {

  // Picker configuration
  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined
    };
  }
  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }

  render() {
    return (
      <Content>
        <View style={styles.planDescriptionRow}>
          <H3 style={styles.planTitleH3}>
            Intensivo Enem
          </H3>
          <View style={styles.planSubDescriptionRow}>
            <Text style={styles.planSubDescriptionBoldText}>
              Mensal
            </Text>
            <Text style={styles.planSubDescriptionText}>
               - R$ 19,90 p/mês
            </Text>
          </View>
          <Text>
              Expira 11/08/2018 (Renov. automática)
          </Text>
        </View>
        <View style={styles.pickerRow}>
          <View style={styles.pickerSelectBoxStyle}>
            <Picker
              mode="dropdown"
              placeholder="Mensal (Renov. Automática)"
              selectedValue={this.state.selected2}
              onValueChange={this.onValueChange2.bind(this)}
            >
              <PickerItem label={creditCardRecurrent} value={creditCardRecurrent} />
              <PickerItem label={creditCardInstallment} value={creditCardInstallment} />
              <PickerItem label={creditCardOnePayment} value={creditCardOnePayment} />
              <PickerItem label={boletoOnePayment} value={boletoOnePayment} />
            </Picker>
          </View>
        </View>
        <Form style={styles.loginContent}>
          <View style={styles.formWrapper}>
            <View style={styles.formItemRow}>
              {/* Pedro: Add prop 'error' to Item when user types an invalid value */}
              <Item floatingLabel style={styles.formItem}>
                <Label>Nome no cartão</Label>
                <Input />
              </Item>
              {/*
                Pedro: Uncomment to show error message.
                <Text style={styles.formItemMessageText}>
                  Formato inválido.
                </Text>
              */}
            </View>
            {/* Valmir: Remove inline if the test works - fix label margin left when icon is visible. */}
            <View style={styles.formItemRow}>
              <Item floatingLabel style={[styles.formItem, {
                flexDirection: 'row',
                position: 'relative',
                paddingLeft: 0,
              }]}>
                <Label style={{
                  flex: 1,
                  marginLeft: 0,
                  paddingLeft: 0,
                  left: 0,
                  justifyContent: 'flex-start', // descobrir onde fica a animação do native-base
                }}>
                  Número do cartão
                </Label>
                <Input />
                <Icon name="ios-card" style={styles.creditCardIcon} />
              </Item>
            </View>
            <Row style={styles.formItemRow}>
              <Col>
                <Item floatingLabel style={styles.formItem}>
                  <Label>D. nascimento</Label>
                  <Input />
                </Item>
              </Col>
              <Col>
                <Item floatingLabel style={styles.formItem}>
                  <Label>CPF</Label>
                  <Input />
                </Item>
              </Col>
            </Row>
            <Row style={styles.formItemRow}>
              <Col>
                <Item floatingLabel style={styles.formItem}>
                  <Label>Expira</Label>
                  <Input />
                </Item>
              </Col>
              <Col>
                <Item floatingLabel style={styles.formItem}>
                  <Label>CVC</Label>
                  <Input />
                </Item>
              </Col>
            </Row>
            {/* Valmir: Replace inline style with remaining time left if any */}
            <ListItem style={{
              borderBottomWidth: 0,
              marginLeft: 16,
            }}>
              <CheckBox />
              <View style={{
                flexDirection: 'row',
                flex: 1,
                marginLeft: 12,
                alignItems: 'flex-start'
              }}>
                <Text style={{
                  flex: 0,
                  fontSize: 14,
                  color: '#666',
                  fontWeight: 'normal',
                }}>
                  {'Aceito'.toUpperCase()}

                </Text>
                <Button transparent style={{
                  flex: 0,
                  paddingLeft: 6,
                  margin: 0,
                }}>
                  <Text style={{
                    fontSize: 14,
                    fontWeight: 'normal',
                  }}>
                    {'os termos de uso'.toLowerCase()}
                  </Text>
                </Button>
              </View>
            </ListItem>
            {/* Move terms to sign up? */}
            <View style={styles.formButtonRow}>
              <Button style={styles.paymentButton}>
                <Text>
                  Realizar pagamento
                </Text>
              </Button>
            </View>
            <View style={styles.paymentHintMessageRow}>
              <Text style={styles.cancelAnyTimeText}>
                Você pode cancelar quando quiser.
              </Text>
            </View>
          </View>
        </Form>
      </Content>
    );
  }
}
