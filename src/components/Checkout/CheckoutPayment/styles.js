/*
  CheckoutPayment styles
 */

import React, { Component } from 'react';
const { StyleSheet } = React;

const screenMarginHor = 40;

export default {
  planSubDescriptionText: {
    marginLeft: 4,
  },
  planSubDescriptionBoldText: {
    fontFamily: 'Roboto-Medium',
  },
  planSubDescriptionRow: {
    flexDirection: 'row',
  },
  planTitleH3: {
    fontFamily: 'Roboto-Medium',
    marginBottom: 6,
  },
  formButtonRow: {
    paddingHorizontal: 12,
    marginVertical: 20,
    margin: 0,
    padding: 0,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  socialButtonsRow: {
    paddingHorizontal: 12,
    marginTop: 40,
    margin: 0,
    padding: 0,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  paymentButton: {
    margin: 0,
    padding: 0,
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#76ad3a',
  },
  googleButton: {
    backgroundColor: '#fd4934',
  },
  formWrapper: {
    paddingHorizontal: screenMarginHor -12,
  },
  pickerRow: {
    paddingHorizontal: screenMarginHor,
    marginBottom: 12,
  },
  planDescriptionRow: {
    paddingHorizontal: screenMarginHor,
    marginVertical: 20,
  },
  formItemRow: {
    paddingRight: 12,
  },
  formRememberButton: {
    justifyContent: 'flex-end',
  },
  formRememberButtonText: {
    color: '#50c0b5',
  },
  formItemMessageText: {
    fontSize: 11,
    paddingHorizontal: 14,
    marginTop: 10,
    color: '#c12f1e',
  },
  creditCardIcon: {
    position: 'absolute',
    right: 0,
    top: 20,
    fontSize: 30,
    color: '#999',
  },
  paymentHintMessageRow: {
    paddingHorizontal: 12,
    marginBottom: 10,
    margin: 0,
    padding: 0,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cancelAnyTimeText: {
    fontSize: 12,
    color: '#666',
  },
  pickerSelectBoxStyle: {
    backgroundColor: '#f0f0f0',
    borderRadius: 5,
    elevation: 1,
  },
  formItem: {
    height: 60,
    marginBottom: 6,
  },
};
