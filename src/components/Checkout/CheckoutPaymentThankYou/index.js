/*
  CheckoutPaymentThankYou
*/

import React, { Component } from 'react';
import styles from "./styles";
import {
  StyleSheet,
  View,
  Image,
} from 'react-native';

import {
  Text,
  Button,
  H2,
  Content,
} from 'native-base';

const onboarding1Uri = require('../../../../resources/images/estudando-hero.png');

export default class CheckoutPaymentThankYou extends Component {
  render() {
    return (
      <Content>
        <View style={styles.contentWrapper}>
          <View style={styles.imageWrapper}>
            {/* Valmir: Replace with xxhdi image of the rocket */}
            <Image source={onboarding1Uri} style={{height: 240, width: 392, flex: 0}}/>
          </View>
          <H2 style={styles.titleH2}>
            Pagamento aprovado!
          </H2>
          <Text style={styles.messageText}>
            Agora você possui acesso ilimitado a todas as videoaulas do seu plano.
          </Text>
          <Button block light style={styles.buttonTertiary}>
            <Text>
            Ir para o guia de estudos
          </Text>
          </Button>
        </View>
      </Content>
    );
  }
}
