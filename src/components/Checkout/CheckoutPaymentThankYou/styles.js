/*
  CheckoutPaymentThankYou styles
 */

import React, { Component } from 'react';
import variables from "../../../theme/variables/platform";

const { StyleSheet } = React;

export default {
  contentWrapper: {
    paddingHorizontal: variables.screenMarginHor,
    alignItems: 'center',
    backgroundColor: '#000',
    flex: 1, // wtf?
  },
  messageText: {
    textAlign: 'center',
    marginTop: 10,
    color: '#fff',
  },
  titleH2: {
    color: '#fff',
    fontFamily: 'Roboto-Light',
  },
};
