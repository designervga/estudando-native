/*
  DisciplinesCollapsibleTopicsList  ### Keep move this code to HomeScreen? ### Use DisciplinesCollapsibleTopicsList instead?
*/

import React, { Component } from 'react';
import styles from "./styles";
import { Col, Row, Grid } from 'react-native-easy-grid';

import {
  StyleSheet,
} from 'react-native';

import {
  List,
  Text,
  ListItem,
  H3,
  Left,
  Right,
} from 'native-base';

const topics = [
  {
    topicName: 'Logaritmos',
    teacher: 'Prof. Carlos henrique',
  },
  {
    topicName: 'Função Logartmica',
    teacher: 'Prof. Carlos henrique',
  },
  {
    topicName: 'Função e Equação Logarítmica',
    teacher: 'Prof. Carlos henrique',
  },
  {
    topicName: 'Exercícios de Exponencial',
    teacher: 'Prof. Carlos henrique',
  },
  {
    topicName: 'Exercícios de logaritmos',
    teacher: 'Prof. Carlos henrique',
  },
];

export default class DisciplinesCollapsibleTopicsList extends Component {
  render() {
    return (
      <List
        dataArray={topics}
        renderRow={topics =>
          <ListItem style={styles.DisciplineTopicsListItem}>
            <Grid>
              <Row>
                <Left>
                  <H3 style={styles.topicNameH3}>
                    {topics.topicName}
                  </H3>
                </Left>
              </Row>
              <Row style={{marginTop: 5}}>
                <Left>
                  <Text style={styles.topicTeacherText}>
                    {topics.teacher}
                  </Text>
                </Left>
                <Right>
                  <Text style={styles.topicTeacherText}>
                    32 min
                  </Text>
                </Right>
              </Row>
              <Row>
                <Text style={{fontSize: 10}}>
                  Importar meterHorbar
                </Text>
              </Row>
            </Grid>
          </ListItem>}
      />
    );
  }
}
