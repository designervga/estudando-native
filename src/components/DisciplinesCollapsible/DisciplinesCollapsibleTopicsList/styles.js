/*
  DisciplinesCollapsibleTopicsList styles
*/

import React, { Component } from 'react';

const { StyleSheet } = React;

export default {
  DisciplineTopicsListItem: {
    marginLeft: 53,
    borderBottomWidth: 0,
    paddingRight: 30,
    paddingBottom: 20,
    paddingTop: 20,
  },
  topicTeacherText: {
    color: '#999',
    margin: 0,
    marginLeft: 0,
  },
  topicNameH3: {
    fontSize: 20,
    lineHeight: 24,
  },
};
