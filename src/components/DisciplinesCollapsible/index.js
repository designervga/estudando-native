/*
  DisciplinesCollapsible
*/

import React, { Component } from 'react';
import styles from "./styles";
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import { Col, Row, Grid } from 'react-native-easy-grid';
import DisciplinesCollapsibleTopicsList from './DisciplinesCollapsibleTopicsList';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../selection.json';
import Ionicon from 'react-native-vector-icons/Ionicons';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';

import {
  Right,
  Left,
} from 'native-base';

const DisciplineContent = [
  {
    title: 'Lín',
    progress: '0/6',
    name: 'linguas',
    colorName: '#25b5ab',
  },
  {
    title: 'Bio',
    progress: '0/6',
    name: 'biologia',
    colorName: '#126193',
  },
  {
    title: 'Mat',
    progress: '6/6',
    name: 'matematica',
    colorName: '#ab4480',
  },
  {
    title: 'His',
    progress: '2/6',
    name: 'historia',
    colorName: '#d4645a',
  },
  {
    title: 'Geo',
    progress: '3/6',
    name: 'geografia',
    colorName: '#6f509c',
  },
  {
    title: 'Fís',
    progress: '7/12',
    name: 'fisica',
    colorName: '#193c7a',
  },
  {
    title: 'Por',
    progress: '0/6',
    name: 'portugues',
    colorName: '#fde683',
  },
  {
    title: 'P. Especiais',
    progress: '0/6',
    name: 'especial',
    colorName: '#272929',
  },
  {
    title: 'Qui',
    progress: '0/6',
    name: 'quimica',
    colorName: '#ae3b3c',
  },
  {
    title: 'Red',
    progress: '0/6',
    name: 'redacao',
    colorName: '#5bb779',
  },
  {
    title: 'Fil&Soc',
    progress: '0/6',
    name: 'filosofia-sociologia',
    colorName: '#2da3b9',
  },
];


export default class DisciplinesCollapsible extends Component {

  state = {
    activeSection: false,
    collapsed: true,
  };

  _toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed });
  }

  _setSection(section) {
    this.setState({ activeSection: section });
  }

  _renderHeader(section, i, isActive) {
    return (
      <View style={[styles.disciplineTouchable, isActive ? styles.headerActive : styles.inactive]}>
        <Grid>
          <Left>
            <Row>
              <Col style={{ width: 80, alignItems: 'center', justifyContent: 'center' }}>
                <Icon name={section.name} size={50} color={section.colorName}/>
              </Col>
              <Col>
                <Text style={styles.headerText}>
                  <Text style={{color: section.colorName}}>{section.title}</Text>
                </Text>
                <Text style={styles.progressText}>
                  <Text style={{color: section.colorName}}>{section.progress}</Text>
                </Text>
              </Col>
            </Row>
          </Left>
          <Right style={{flex: 0}}>
            <Ionicon name="md-arrow-dropdown" color="#333"/>
          </Right>
        </Grid>
      </View>
    );
  }

  _renderContent(section, i, isActive) {
    return (
      <View style={[styles.disciplineCollapsible, isActive ? styles.active : styles.inactive]}>
        <DisciplinesCollapsibleTopicsList />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Accordion
          activeSection={this.state.activeSection}
          sections={DisciplineContent}
          renderHeader={this._renderHeader}
          renderContent={this._renderContent}
          duration={400}
          onChange={this._setSection.bind(this)}
        />
      </View>
    );
  }
}
