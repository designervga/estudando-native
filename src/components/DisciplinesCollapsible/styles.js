/*
  DisciplinesCollapsible styles
 */

import React, { Component } from 'react';

const { StyleSheet } = React;

export default {
  container: {
    flex: 1,
  },
  disciplineTouchable: {
    paddingTop: 30,
    paddingBottom: 30,
    borderBottomWidth: 1,
    borderBottomColor: '#f4f3f3',
    flex: 1,
  },
  headerActive: {
    paddingTop: 30,
    paddingBottom: 30,
    borderBottomWidth: 0,
    backgroundColor: 'rgba(255,255,255,1)',
  },
  headerText: {
    textAlign: 'left',
    fontSize: 50,
    fontWeight: '600',
    color: '#000000',
    marginBottom: 0,
    padding: 0,
    lineHeight: 50,
  },
  progressText: {
    fontWeight: '500',
    marginTop: 0,
    padding: 0,
  },
  disciplineCollapsible: {
    padding: 30,
    backgroundColor: '#fff',
    paddingTop: 30,
    paddingBottom: 60,
    borderBottomWidth: 1,
    borderBottomColor: '#f4f3f3',
  },
  active: {
    backgroundColor: 'rgba(255,255,255,1)',
  },
  inactive: {
    backgroundColor: 'rgba(255,255,255,1)',
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selector: {
    backgroundColor: '#000000',
    padding: 10,
  },
  activeSelector: {
    fontWeight: 'bold',
  },
};
