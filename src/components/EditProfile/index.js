/*
  EditProfile
*/

import React, { Component } from 'react';
import styles from "./styles";
import { Grid, Row, Col } from 'react-native-easy-grid';
import Ionicon from 'react-native-vector-icons/Ionicons';

import {
  View,
  StyleSheet,
} from 'react-native';

import {
  Content,
  Text,
  Form,
  Item,
  PickerItem as FormItem,
  Input,
  Label,
  Picker,
} from 'native-base';

const PickerItem = Picker.Item;

export default class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected1: "key1"
    };
  }
  onValueChange(value: string) {
    this.setState({
      selected1: value
    });
  }

  render() {
    return (
      <Content>
        <Form style={{paddingRight: 16}}>
          <Item floatingLabel >
            <Label>Nome completo</Label>
            <Input />
          </Item>
          <Item floatingLabel>
            <Label>Email</Label>
            <Input />
          </Item>
          <Row style={styles.subheading}>
            <Text style={styles.subheadingText}>
              Interesse principal
            </Text>
          </Row>
          <View style={styles.pickerGrid}>
            <Picker
                iosHeader="Selecione"
                mode="dropdown"
                placeholder="Selecione"
                selectedValue={this.state.selected1}
                onValueChange={this.onValueChange.bind(this)}
              >
                <PickerItem label="Gosto de tudo" value="key0" />
                <PickerItem label="Ciências Biológicas" value="key1" />
                <PickerItem label="Ciências Exatas" value="key2" />
                <PickerItem label="Ciências Humanas" value="key3" />
            </Picker>
          </View>
        </Form>
      </Content>
    );
  }
}
