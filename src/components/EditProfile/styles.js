/*
  EditProfile styles
 */

import React, { Component } from 'react';
import variables from "../../theme/variables/platform";

const { StyleSheet } = React;

export default {
  profileIcon: {
    flex: 0,
    width: 56,
    alignItems: 'center',
  },
  pickerGrid: {
    marginTop: 0,
    marginLeft: 12,
    padding: 0,
    borderColor: '#D9D5DC',
    borderBottomWidth: 0.66,
  },
  subheading: {
    marginTop: 24,
    marginLeft: 15,
    marginBottom: 0,
  },
  subheadingText: {
    color: '#999999',
  },
};
