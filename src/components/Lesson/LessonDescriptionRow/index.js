/*
  LessonDescriptionRow - Visible in portrait orientation.
*/

import React, { Component } from 'react';
import styles from "./styles";
import { Grid, Row } from 'react-native-easy-grid';
import Ionicon from 'react-native-vector-icons/Ionicons';

import {
  StyleSheet,
} from 'react-native';

import {
  Text,
  Left,
  Body,
  Button,
  Right,
  Content,
} from 'native-base';

export default class LessonDescriptionRow extends Component {
  render() {
    return (
      <Grid style={styles.lessonDescriptionRow}>
        <Left>
          <Text style={styles.lessonDescriptionText}>
            Descrição da video aula
          </Text>
          <Text note style={styles.lessonTeacherText}>
            Prof. Carlos Henrique
          </Text>
        </Left>
        <Right>
          <Button transparent success>
            <Ionicon name="md-more" size={22} style={styles.lessonMoreIcon} />
            {/* On click opens Native-base actionsheet: 'Mark as watched' and 'Share lesson' */}
            {/* http://docs.nativebase.io/Components.html#actionsheet-def-headref */}
          </Button>
        </Right>
      </Grid>
    );
  }
}
