/*
  LessonDescriptionRow styles
 */

import React, { Component } from 'react';

const { StyleSheet } = React;

export default {
  lessonDescriptionText: {
    color: '#fff',
  },
  lessonTeacherText: {
    color: '#999',
  },
  lessonDescriptionRow: {
    marginTop: 24,
    paddingLeft: 16,
    paddingRight: 16,
  },
  lessonMoreIcon: {
    color: '#fff',
  }
};
