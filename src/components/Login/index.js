/*
  Login
*/

import React, { Component } from 'react';
import styles from "./styles";

import {
  View,
} from 'react-native';

import {
  Text,
  Button,
  Content,
  Form,
  Item,
  Input,
  Icon,
  Label,
} from 'native-base';

export default class Login extends Component {
  render() {
    return (
      <Content>
        <Form style={styles.loginContent}>
          <View style={styles.formWrapper}>
            <View style={styles.formItemWrapper}>
              {/* Add prop 'error' to Item when user types an invalid value */}
              <Item floatingLabel style={styles.formItem}>
                <Label>Seu e-mail</Label>
                <Input />
              </Item>
              {/*
                Uncomment to show error message.
                <Text style={styles.formItemMessageText}>
                  Formato inválido.
                </Text>
              */}
            </View>
            <View style={styles.formItemWrapper}>
              <Item floatingLabel style={styles.formItem}>
                <Label>Sua senha</Label>
                <Input />
              </Item>
            </View>
            <View style={styles.formButtonRow}>
              <Button transparent style={[styles.formButton, styles.formRememberButton]}>
                <Text style={styles.formRememberButtonText}>
                  Lembrar senha
                </Text>
              </Button>
            </View>
            <View style={styles.formButtonRow}>
              <Button style={styles.formButton}>
                <Text>
                  Entrar
                </Text>
              </Button>
            </View>
          </View>
        </Form>
        <View style={styles.formWrapper}>
          <View style={styles.socialButtonsRow}>
            <Text style={styles.socialWrapperHeading}>
              Acesse com:
            </Text>
            <Button style={[styles.socialButton, styles.facebookButton]}>
              <Text>
                Facebook
              </Text>
            </Button>
            <Button style={[styles.socialButton, styles.googleButton]}>
              <Text>
                Google
              </Text>
            </Button>
          </View>
        </View>
      </Content>
    );
  }
}
