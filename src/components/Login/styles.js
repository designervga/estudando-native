/*
  Login styles
 */

import React, { Component } from 'react';

const { StyleSheet } = React;

export default {
  formButtonRow: {
    paddingHorizontal: 12,
    marginVertical: 20,
    margin: 0,
    padding: 0,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  socialButtonsRow: {
    paddingHorizontal: 12,
    marginTop: 40,
    margin: 0,
    padding: 0,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  formButton: {
    margin: 0,
    padding: 0,
    flex: 1,
    justifyContent: 'center',
  },
  socialButton: {
    margin: 0,
    padding: 0,
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'stretch',
  },
  facebookButton: {
    marginBottom: 8,
    backgroundColor: '#245a9f',
  },
  googleButton: {
    backgroundColor: '#fd4934',
  },
  formWrapper: {
    paddingHorizontal: 30,
  },
  formItemWrapper: {
    paddingRight: 12,
  },
  formRememberButton: {
    justifyContent: 'flex-end',
  },
  formRememberButtonText: {
    color: '#50c0b5',
  },
  formItemMessageText: {
    fontSize: 11,
    paddingHorizontal: 14,
    marginTop: 10,
    color: '#c12f1e',
  },
  socialWrapperHeading: {
    fontSize: 16,
    marginBottom: 12,
    alignSelf: 'flex-start',
  }
};
