/*
  Onboarding
*/

import React, { Component } from 'react';
import styles from "./styles";
import Swiper from './Swiper';
import {
  ImageBackground,
  View,
} from 'react-native';

import {
  Text,
  Container,
} from 'native-base';

const bgLastSlide = require('../../../resources/images/splash-bio-xxhdpi.png');
const onboarding1Uri = require('../../../resources/images/estudando-hero.png');
const onboarding2Uri = require('../../../resources/images/metodo.png');
const onboarding3Uri = require('../../../resources/images/professores.png');
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../selection.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class Onboarding extends Component {
  render() {
    return (
      <Swiper>
        <Container style={[styles.slide, { backgroundColor: '#54bbf6' }]}>
          <View style={styles.imageWrapper}>
            <ImageBackground source={onboarding1Uri} style={{height: 240, width: 392, flex: 0}}/>
          </View>
          <Text style={styles.pitchHeaderText}>
            Guia
          </Text>
          <Text style={styles.pitchText}>
            Nosso guia de estudo indica o que estudar a cada semana.
          </Text>
        </Container>
        <Container style={[styles.slide, { backgroundColor: '#25b5ab' }]}>
          <View style={styles.imageWrapper}>
            <ImageBackground source={onboarding2Uri} style={{height: 187, width: 160, flex: 0}}/>
          </View>
          <Text style={styles.pitchHeaderText}>
            Ferramentas
          </Text>
          <Text style={styles.pitchText}>
            Contratamos os melhores profissionais para fazerem parte de nossa equipe.
          </Text>
        </Container>
        <Container style={[styles.slide, { backgroundColor: '#2da3b9' }]}>
          <View style={styles.imageWrapper}>
            <ImageBackground source={onboarding3Uri} style={{height: 187, width: 160, flex: 0}}/>
          </View>
          <Text style={styles.pitchHeaderText}>
            Professores
          </Text>
          <Text style={styles.pitchText}>
            Contratamos os melhores profissionais para fazerem parte de nossa equipe.
          </Text>
        </Container>
        <Container style={[styles.slide, { backgroundColor: '#02a7e3' }]}>
          <ImageBackground style={styles.fullScreen} source={bgLastSlide}>
            <View style={styles.imageWrapper}>
              <Icon name="estudando-logo-1" color="#fff" size={180}/>
            </View>
            <Text style={styles.pitchHeaderText}>
            </Text>
            <Text style={styles.pitchText}>
            </Text>
          </ImageBackground>
        </Container>
      </Swiper>
    );
  }
}
