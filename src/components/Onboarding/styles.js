/*
  Onboarding styles
 */

import React, { Component } from 'react';

const { StyleSheet } = React;

import {
  Dimensions,
} from 'react-native';


// Detect screen width and height
const { width, height } = Dimensions.get('window');

export default {
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pitchHeaderText: {
    color: '#FFFFFF',
    fontSize: 26,
    fontWeight: 'bold',
    marginVertical: 6,
    textAlign: 'center',
  },
  pitchText: {
    color: '#FFFFFF',
    fontSize: 20,
    fontFamily: 'Roboto-Light',
    marginHorizontal: 60,
    lineHeight: 32,
    paddingHorizontal: 20,
    textAlign: 'center',
  },
  fullScreen: {
    width: width,
    height: height,
  },
  container: {
    backgroundColor: 'transparent',
    position: 'relative',
    alignItems: 'center',
  },
  slide: {
    backgroundColor: 'transparent',
  },
  pagination: {
    position: 'absolute',
    bottom: 150,
    left: 0,
    right: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    backgroundColor: 'transparent',
  },
  dot: {
    backgroundColor: 'rgba(255,255,255,.25)',
    width: 7,
    height: 7,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3,
  },
  activeDot: {
    backgroundColor: '#FFFFFF',
  },
  buttonWrapper: {
    backgroundColor: 'transparent',
    flexDirection: 'column',
    position: 'absolute',
    bottom: 0,
    left: 0,
    flex: 1,
    paddingHorizontal: 50,
    paddingVertical: 70,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  signupbButtonView: {
    flex: 0,
  },
  signinButton: {
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderRadius: 50,
    borderColor: '#fff',
    marginTop: 16,
    elevation: 0,
    height: 50,
  },
  continueButton: {
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderRadius: 50,
    borderColor: '#fff',
    elevation: 0,
    height: 50,
  },
  signupButton: {
    backgroundColor: '#fff',
    borderWidth: 2,
    borderRadius: 50,
    borderColor: '#fff',
    height: 50,
    elevation: 0,
  },
  signupButtonText: {
    color: '#196793',
  },
  imageWrapper: {
    height: 300,
    flex: 0,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
};
