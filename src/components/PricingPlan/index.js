/*
  PricingPlan
*/

import React, { Component } from 'react';
import styles from "./styles";
import { Grid, Row, Col } from 'react-native-easy-grid';

import {
  StyleSheet,
  View,
} from 'react-native';

import {
  Text,
  Card,
  CardItem,
  Button,
  Body,
  H2,
  H3,
  Form,
  Item as FormItem,
  Picker,
  Content,
} from 'native-base';


// Payment methods
const Item = Picker.Item;
const monthlyPaymentsWithRenovation = "Mensal (Renov. Automática)"
const yearlyOnePayment = "Anual à vista (12m de acesso)"
const yearlyInstallmentPayments = "Anual 12x (12m de acesso)"
const limitedOnePayment = "Limitado à vista (de 31/08 a 07/09)"

export default class PricingPlan extends Component {

  // Picker configuration
  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined
    };
  }
  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }

  render() {
    return (
      <Content>
        <Card style={{elevation: 4, borderRadius: 6, shadowColor: "#ff00ff"}}>
          <CardItem header  style={styles.planHeaderCardItem}>
            <H2 style={styles.planHeaderH2}>
              Intensivo Enem
            </H2>
          </CardItem>
          <View style={styles.pickerRow}>
            <Picker
              mode="dropdown"
              placeholder="Mensal (Renov. Automática)"
              selectedValue={this.state.selected2}
              onValueChange={this.onValueChange2.bind(this)}
            >
              <Item label={monthlyPaymentsWithRenovation} value={monthlyPaymentsWithRenovation} />
              <Item label={yearlyOnePayment} value={yearlyOnePayment} />
              <Item label={yearlyInstallmentPayments} value={yearlyInstallmentPayments} />
              <Item label={limitedOnePayment} value={limitedOnePayment} />
            </Picker>
          </View>
          <CardItem>
            <Body style={styles.planDescriptionBody}>
              <H3 style={styles.planPricingH3}>
                R$ 19,90 p/ mês
              </H3>
              <Text style={styles.planDescriptionText}>
                6 aulas ao vivo por mês{"\n"}
                Acesso às aulas gravadas{"\n"}
                Guia por disciplinas{"\n"}
                Guia por área de conhecimento{"\n"}
                Exercícios e materiaos de apoio{"\n"}
                2 monitorias por semana{"\n"}
              </Text>
            </Body>
          </CardItem>
          <CardItem footer style={styles.footerCardItem}>
            <View style={styles.planActionsRow}>
              <Button block>
                <Text>
                  Prosseguir com pagamento
                </Text>
              </Button>
              <Button block light style={styles.planSkipButton}>
                <Text>
                  Quero pagar depois
                </Text>
              </Button>
            </View>
          </CardItem>
        </Card>
      </Content>
    );
  }
}
