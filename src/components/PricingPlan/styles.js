/*
  PricingPlan styles
 */

import React, { Component } from 'react';

const { StyleSheet } = React;

export default {
  planDescriptionText: {
    fontSize: 16,
    color: '#999',
    lineHeight: 30,
    alignSelf: 'center',
    textAlign: 'center',
  },
  planDescriptionBody: {
    paddingTop: 20,
  },
  planHeaderCardItem: {
    height: 90,
    backgroundColor: '#029bd3',
    flex: 1,
    borderRadius: 6,
    justifyContent: 'center',
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
  },
  footerCardItem: {
    borderRadius: 6,
    paddingBottom: 25,
  },
  planHeaderH2: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
  },
  planPricingH3: {
    fontSize: 20,
    alignSelf: 'center',
    marginBottom: 8,
    fontSize: 24,
    lineHeight: 30,
  },
  pickerRow: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    margin: 0,
    borderColor: '#D9D5DC',
    borderBottomWidth: 0.66,
    flex: 1,
  },
  planActionsRow: {
    flex: 1,
  },
  planSkipButton: {
    marginTop: 16,
  },
};
