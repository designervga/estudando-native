/*
  PricingPlanCarousel
*/

import React, { Component } from 'react';
import styles from "./styles";
import Carousel, { Pagination } from 'react-native-snap-carousel';
import PricingPlan from '../PricingPlan'

import {
  StyleSheet,
  View,
  Dimensions,
} from 'react-native';

import {
  Text,
} from 'native-base';

const horizontalMargin = 20;
const slideWidth = 300;

const sliderWidth = Dimensions.get('window').width;
const itemWidth = slideWidth + horizontalMargin * 2;

export default class PricingPlanCarousel extends Component {

  state = {
        data: [{
          title: 'Falta criar props para esses dados',
          description: 'Falta criar props para esses dados',
          price: 'Falta criar props para esses dados',
        },
        {

        },
        {

        }],
    }

  _renderItem ({item, index}) {
     return (
      <View style={styles.slide}>
        <PricingPlan />
      </View>
     );
   }

  render () {
   return (
     <Carousel
       ref={(c) => { this._carousel = c; }}
       data={this.state.data}
       renderItem={this._renderItem}
       sliderWidth={sliderWidth}
       itemWidth={itemWidth}
     />
   );
  }
}
