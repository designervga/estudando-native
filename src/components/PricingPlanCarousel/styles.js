/*
  PricingPlanCarousel styles
 */

import React, { Component } from 'react';
import { StyleSheet, Dimensions, Platform } from 'react-native';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = 630;
const slideWidth = wp(80);
const itemHorizontalMargin = wp(1);

export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

export default {
  slide: {
    width: itemWidth,
    flex: 0,
    paddingHorizontal: itemHorizontalMargin,
    paddingBottom: 18,
  }
};
