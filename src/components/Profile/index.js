/*
  Profile
*/

import React, { Component } from 'react';
import styles from "./styles";
import { Grid, Row, Col } from 'react-native-easy-grid';
import Ionicon from 'react-native-vector-icons/Ionicons';

import {
  StyleSheet,
} from 'react-native';

import {
  Text,
  Button,
  List,
  ListItem,
  Content,
  Left,
  Body,
  Thumbnail,
} from 'native-base';

export default class Profile extends Component {
  render() {
    return (
      <Content>
        <Grid style={styles.profileGrid}>
          <Left style={styles.profileIcon}>
            <Text style={styles.profileIconText}>
              <Ionicon name="md-contact" size={48}/>
            </Text>
          </Left>
          <Left>
            <Text>NativeBase</Text>
            <Text note>GeekyAnts</Text>
          </Left>
        </Grid>
        {/* Replace the above Grid with the below one when user doesn't have a subscription. */}
        <Grid style={styles.subscribePlanGrid}>
          <Button full light style={{flex: 1, borderRadius: 2}}>
            <Text>
              Assine um plano
            </Text>
          </Button>
        </Grid>
        <List>
          <ListItem avatar style={{marginLeft: 16}}>
            <Left>
              <Text>
                <Ionicon name="md-school" size={24} />
              </Text>
            </Left>
            <Body>
              <Text>Meus Planos</Text>
              <Text note>Enem 2017 - Exprira em 30/10/2017</Text>
            </Body>
          </ListItem>
          <ListItem avatar style={{marginLeft: 16}}>
            <Left>
              <Text>
                <Ionicon name="ios-help-buoy" size={24} />
              </Text>
            </Left>
            <Body>
              <Text>Suporte</Text>
            </Body>
          </ListItem>
          <ListItem avatar style={{marginLeft: 16}}>
            <Left>
              <Text>
                <Ionicon name="md-log-out" size={24} />
              </Text>
            </Left>
            <Body>
              <Text>Sair</Text>
            </Body>
          </ListItem>
        </List>
      </Content>
    );
  }
}
