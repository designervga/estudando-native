/*
  Profile styles
 */

import React, { Component } from 'react';
import variables from "../../theme/variables/platform";

const { StyleSheet } = React;

export default {
  profileIcon: {
    flex: 0,
    width: 56,
    alignItems: 'center',
  },
  profileIconText: {
    color: variables.brandPrimary,
  },
  profileGrid: {
    borderBottomWidth: 1,
    borderBottomColor: '#f4f3f3',
    borderTopWidth: 1,
    borderTopColor: '#f4f3f3',
    padding: 12,
    paddingLeft: 16,
    paddingTop: 24,
    paddingBottom: 24,
    marginTop: -1,
  },
  subscribePlanGrid: {
    backgroundColor: variables.brandPrimary,
    borderBottomWidth: 1,
    borderBottomColor: '#f4f3f3',
    borderTopWidth: 1,
    borderTopColor: '#f4f3f3',
    padding: 12,
    paddingLeft: 16,
    paddingTop: 24,
    paddingBottom: 24,
    marginTop: -1,
  },
};
