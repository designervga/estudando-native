/*
  RememberPassword
*/

import React, { Component } from 'react';
import styles from "./styles";

import {
  View,
} from 'react-native';

import {
  Text,
  Button,
  Content,
  Form,
  Item,
  Input,
  Icon,
  Label,
} from 'native-base';

export default class RememberPassword extends Component {
  render() {
    return (
      <Content>
        <Form style={styles.loginContent}>
          <View style={styles.formWrapper}>
            <View style={styles.formItemWrapper}>
              {/* Add prop 'error' to Item when user types an invalid value */}
              <Item floatingLabel style={styles.formItem}>
                <Label>Digite o e-mail da sua conta</Label>
                <Input />
              </Item>
              {/*
                Uncomment to show error message.
                <Text style={styles.formItemMessageText}>
                  Formato inválido.
                </Text>
              */}
            </View>
            <View style={styles.formButtonRow}>
              <Button style={styles.formButton}>
                <Text>
                  OK
                </Text>
              </Button>
            </View>
          </View>
        </Form>
      </Content>
    );
  }
}
