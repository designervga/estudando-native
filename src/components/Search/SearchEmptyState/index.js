/*
  SearchEmptyState
*/

import React, { Component } from 'react';
import styles from "./styles";
import { Grid, Row } from 'react-native-easy-grid';

import {
  StyleSheet,
} from 'react-native';

import {
  Text,
} from 'native-base';

export default class EmptyState extends Component {
  render() {
    return (
      <Grid style={styles.searchGrid}>
        <Row style={styles.emptyStateMessage}>
          <Text>
            Nenhum resultado encontrado.
          </Text>
        </Row>
      </Grid>
    );
  }
}
