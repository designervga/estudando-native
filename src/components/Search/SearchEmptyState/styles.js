/*
  SearchEmptyState styles
 */

import React, { Component } from 'react';

const { StyleSheet } = React;
import variables from "../../../theme/variables/platform";

export default {
  searchGrid: {
    backgroundColor: variables.screenBackgroundGray90,
  },
  emptyStateMessage: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  }
};
