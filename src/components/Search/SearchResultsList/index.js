/*
  SearchResultsList
*/

import React, { Component } from 'react';
import styles from "./styles";
import SearchResultsListItem from '../SearchResultsListItem';
import {
  StyleSheet,
} from 'react-native';

import {
  Text,
  List,
  ListItem,
} from 'native-base';

export default class SearchResultsList extends Component {
  render() {
    return (
      <List>
        <ListItem itemDivider>
          <Text>Tópicos</Text>
        </ListItem>
        <SearchResultsListItem />
        <SearchResultsListItem />
        <SearchResultsListItem />
        <SearchResultsListItem />
        <ListItem itemDivider>
          <Text>Aulas</Text>
        </ListItem>
        <SearchResultsListItem />
        <SearchResultsListItem />
        <SearchResultsListItem />
        <SearchResultsListItem />
      </List>
    );
  }
}
