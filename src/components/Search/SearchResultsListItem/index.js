/*
  SearchResultsListItem
*/

import React, { Component } from 'react';
import styles from "./styles";

import {
  StyleSheet,
} from 'react-native';

import {
  Text,
  ListItem,
} from 'native-base';

export default class SearchResultsListItem extends Component {
  render() {
    return (
      <ListItem>
        <Text>
          Propriedades do logaritmo
        </Text>
      </ListItem>
    );
  }
}
