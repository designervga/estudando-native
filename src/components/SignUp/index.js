/*
  SignUp
*/

import React, { Component } from 'react';
import styles from "./styles";

import {
  View,
} from 'react-native';

import {
  Text,
  Button,
  Content,
  Form,
  Item,
  Input,
  Icon,
  Label,
} from 'native-base';

export default class SignUp extends Component {
  render() {
    return (
      <Content>
        <Form style={styles.loginContent}>
          <View style={styles.formWrapper}>
            <View style={styles.formItemWrapper}>
              {/* Add prop 'error' to Item when user types an invalid value */}
              <Item floatingLabel style={styles.formItem}>
                <Label>E-mail</Label>
                <Input />
              </Item>
              {/*
                Uncomment to show error message.
                <Text style={styles.formItemMessageText}>
                  Formato inválido.
                </Text>
              */}
            </View>
            <View style={styles.formItemWrapper}>
              <Item floatingLabel style={styles.formItem}>
                <Label>Senha</Label>
                <Input />
              </Item>
            </View>
            <View style={styles.formItemWrapper}>
              <Item floatingLabel style={styles.formItem}>
                <Label>Confirmar senha</Label>
                <Input />
              </Item>
            </View>
            <View style={styles.formButtonRow}>
              <Button style={styles.formButton}>
                <Text>
                  Criar conta
                </Text>
              </Button>
            </View>
          </View>
        </Form>
        <View style={styles.formWrapper}>
          <View style={styles.socialButtonsRow}>
            <Text style={styles.socialWrapperHeading}>
              Acesse com:
            </Text>
            <Button style={[styles.socialButton, styles.facebookButton]}>
              <Text>
                Facebook
              </Text>
            </Button>
            <Button style={[styles.socialButton, styles.googleButton]}>
              <Text>
                Google
              </Text>
            </Button>
          </View>
        </View>
      </Content>
    );
  }
}
