/*
  TopicExerciseListItem
*/

import React, { Component } from 'react';
import styles from "./styles";
import TopicLessonMeter from '../TopicLessonMeter';
import Ionicon from 'react-native-vector-icons/Ionicons';

import {
  StyleSheet,
} from 'react-native';

import {
  Text,
  List,
  ListItem,
  Left,
  Right,
  Body,
} from 'native-base';

export default class TopicExerciseListItem extends Component {
  render() {
    return (
      <ListItem avatar style={{marginLeft: 16}}>
        <Left style={{width: 52}}>
          <Text>
            <TopicLessonMeter />
          </Text>
        </Left>
        <Body style={{marginLeft: 16}}>
          <Text>
            Propriedades do logaritmo
          </Text>
        </Body>
        <Right>
          <Ionicon name="md-copy" size={24} />
        </Right>
      </ListItem>
    );
  }
}
