/*
  TopicLessonList
*/

import React, { Component } from 'react';
import styles from "./styles";
import TopicLessonListItem from '../TopicLessonListItem';
import TopicExerciseListItem from '../TopicExerciseListItem';
import {
  StyleSheet,
} from 'react-native';

import {
  List,
  ListItem,
  Text,
} from 'native-base';

export default class TopicLessonList extends Component {
  render() {
    return (
      <List>
        <TopicLessonListItem />
        <TopicLessonListItem />
        <TopicLessonListItem />
        <TopicLessonListItem />
        <ListItem itemHeader first style={{marginTop: 20, paddingBottom: 0}}>
          <Text>Exercícios</Text>
        </ListItem>
        <TopicExerciseListItem />
        <TopicExerciseListItem />
        <TopicExerciseListItem />
        <TopicExerciseListItem />
      </List>
    );
  }
}
