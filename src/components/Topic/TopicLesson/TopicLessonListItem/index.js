/*
  TopicLessonListItem
*/

import React, { Component } from 'react';
import styles from "./styles";
import TopicLessonMeter from '../TopicLessonMeter';
import {
  StyleSheet,
} from 'react-native';

import {
  Text,
  List,
  ListItem,
  Left,
  Body,
} from 'native-base';

export default class TopicLessonListItem extends Component {
  render() {
    return (
      <ListItem avatar style={{marginLeft: 16}}>
        <Left style={{width: 52}}>
          <Text>
            <TopicLessonMeter />
          </Text>
        </Left>
        <Body style={{marginLeft: 16}}>
          <Text>
            Propriedades do logaritmo
          </Text>
        </Body>
      </ListItem>
    );
  }
}
