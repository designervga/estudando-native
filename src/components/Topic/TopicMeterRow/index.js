/*
  Topic > TopicMeterRow
*/

import React, { Component } from 'react';
import styles from "./styles";
import { Grid, Row, Col } from 'react-native-easy-grid';

import {
  StyleSheet,
} from 'react-native';

import {
  Text,
  Card,
} from 'native-base';

export default class TopicMeterRow extends Component {
  render() {
    return (
      <Card style={styles.wrapper}>
        <Grid>
          <Row>
            <Col>
              <Text style={styles.brandPrimary}>
                2/6
              </Text>
            </Col>
            <Text style={styles.gray80}>
              36 min
            </Text>
          </Row>
          <Row>
            <Text>
              Add hor. progress bar
            </Text>
          </Row>
        </Grid>
      </Card>
    );
  }
}
