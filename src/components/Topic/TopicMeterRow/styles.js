/*
  Topic > TopicMeterRow
 */

import React, { Component } from 'react';
import variables from "../../../theme/variables/platform";

const { StyleSheet } = React;

export default {
  wrapper: {
    padding: 25,
    borderRadius: 0,
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
    elevation: 5,
    shadowOpacity: .1, /* not working */
    shadowOffset: { width: 0, height: 2 }, /* not working */
    shadowColor: 'red', /* not working */
    shadowRadius: 1.5, /* not working */
  },
  gray80: {
    color: variables.textColorGray90,
  },
  brandPrimary: {
    color: variables.brandPrimary,
  }
};
