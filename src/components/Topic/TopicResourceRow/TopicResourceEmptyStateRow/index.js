/*
  Topic > TopicResourceEmptyStateRow
*/

import React, { Component } from 'react';
import styles from "./styles";
import { Col, Row, Grid } from 'react-native-easy-grid';
import Ionicon from 'react-native-vector-icons/Ionicons';

import {
  StyleSheet,
} from 'react-native';

import {
  Left,
  Text,
  Body,
} from 'native-base';

export default class TopicResourceEmptyStateRow extends Component {
  render() {
    return (
      <Grid style={styles.resourcesRow}>
        <Col style={{width: 72, alignItems: "center"}}>
          <Text style={styles.grayed}>
            <Ionicon name="md-copy" size={24} />
          </Text>
        </Col>
        <Col>
          <Text style={styles.grayed}>
            Material de Apoio
          </Text>
        </Col>
      </Grid>
    );
  }
}
