/*
  Topic > TopicResourceRow
 */

import React, { Component } from 'react';

const { StyleSheet } = React;

export default {
  resourcesRow: {
    borderBottomWidth: 1,
    borderBottomColor: '#f4f3f3',
    borderTopWidth: 1,
    borderTopColor: '#f4f3f3',
    padding: 12,
    paddingTop: 24,
    paddingBottom: 24,
    marginTop: -1,
  }
};
