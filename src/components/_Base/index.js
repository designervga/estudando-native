/*
  Base
*/

import React, { Component } from 'react';
import styles from "./styles";
import {
  StyleSheet,
} from 'react-native';

import {
  Text,
} from 'native-base';

export default class Base extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>I'm the MyComponent component</Text>
      </View>
    );
  }
}
