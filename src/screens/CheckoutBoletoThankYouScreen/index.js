/*
  CheckoutPaymentThankYouScreen
*/

import React, { Component } from 'react';
import styles from "./styles";
import CheckoutBoletoThankYou from '../../components/Checkout/CheckoutBoletoThankYou';

import {
  StyleSheet,
} from 'react-native';

import {
  Content,
} from 'native-base';

export default class CheckoutBoletoThankYouScreen extends Component {
  render() {
    return (
      <Content>
        <CheckoutBoletoThankYou />
      </Content>
    );
  }
}
