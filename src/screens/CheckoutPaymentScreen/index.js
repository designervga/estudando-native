/*
  CheckoutPaymentScreen
*/

import React, { Component } from 'react';
import styles from "./styles";
import CheckoutPayment from '../../components/Checkout/CheckoutPayment';

import {
  StyleSheet,
} from 'react-native';

import {
  Content,
} from 'native-base';

export default class CheckoutPaymentScreen extends Component {
  render() {
    return (
      <Content>
        <CheckoutPayment />
      </Content>
    );
  }
}
