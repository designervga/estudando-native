/*
  CheckoutPaymentThankYouScreen
*/

import React, { Component } from 'react';
import styles from "./styles";
import CheckoutPaymentThankYou from '../../components/Checkout/CheckoutPaymentThankYou';

import {
  StyleSheet,
} from 'react-native';

import {
  Content,
} from 'native-base';

export default class CheckoutPaymentThankYouScreen extends Component {
  render() {
    return (
      <Content>
        <CheckoutPaymentThankYou />
      </Content>
    );
  }
}
