/*
  DisciplinesScreen
*/

import React, { Component } from 'react';
import styles from "./styles";
import DisciplinesList from '../../components/Disciplines/DisciplinesList';

import {
  StyleSheet,
} from 'react-native';

import {
  Content,
  Text,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
} from 'native-base';

export default class DisciplinesScreen extends Component {
  render() {
    return (
      <Content>
        <DisciplinesList />
      </Content>
    );
  }
}
