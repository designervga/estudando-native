/*
  EditProfileScreen
*/

import React, { Component } from 'react';
import styles from "./styles";
import EditProfile from '../../components/EditProfile';

import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

export default class EditProfileScreen extends Component {
  render() {
    return (
      <EditProfile />
    );
  }
}
