/*
  HomeScreen
*/

import React, { Component } from 'react';
import styles from "./styles";

import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

export default class HomeScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>
          Import 4 main tabs.
        </Text>
      </View>
    );
  }
}
