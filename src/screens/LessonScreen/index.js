/*
  LessonScreen
*/

import React, { Component } from 'react';
import styles from "./styles";
import LessonVideo from '../../components/Lesson/LessonVideo';
import LessonDescriptionRow from '../../components/Lesson/LessonDescriptionRow';

import {
  Text,
  StyleSheet,
} from 'react-native';

import {
  Content,
} from 'native-base';

export default class LessonScreen extends Component {
  render() {
    return (
      <Content style={styles.lessonScreenContent}>
        <LessonVideo />
        <LessonDescriptionRow />
      </Content>
    );
  }
}
