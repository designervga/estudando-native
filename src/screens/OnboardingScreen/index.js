/*
  OnboardingScreen
*/

import React, { Component } from 'react';
import styles from "./styles";
import Onboarding from '../../components/Onboarding';

import {
  StyleSheet,
} from 'react-native';

export default class OnboardingScreen extends Component {
  render() {
    return (
      <Onboarding />
    );
  }
}
