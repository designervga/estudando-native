/*
  PlansScreen
*/

import React, { Component } from 'react';
import styles from "./styles";
import PricingPlanCarousel from '../../components/PricingPlanCarousel';

import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import {
  Content,
  H3,
  Button,
} from 'native-base';

export default class PlansScreen extends Component {
  render() {
    return (
      <Content>
        <View style={styles.plansRow}>
          <H3 style={styles.plansHeadingH3}>
            Escolha um {"\n"}
            objetivo para 2018
          </H3>
        </View>
        <PricingPlanCarousel />
        <View style={styles.couponRow}>
          <Button block transparent>
            <Text style={styles.couponButtonText}>
              {'Possui um cupom?'.toUpperCase()}
            </Text>
          </Button>
        </View>
        <View style={styles.plansTrialMessageRow}>
          <Text style={styles.couponHeadingText}>
            Experimente com 7 dias grátis!
          </Text>
          <Text style={styles.couponDescriptionText}>
            Se não ficar satisfeito você pode cancelar a qualquer momento dos primeiros 7 dias, sem custo.
          </Text>
        </View>
      </Content>
    );
  }
}
