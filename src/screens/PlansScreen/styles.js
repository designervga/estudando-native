/*
  PlansScreen
*/

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    plansHeadingH3: {
      fontSize: 30,
      fontFamily: 'Roboto-Light',
      lineHeight: 36,
    },
    plansRow: {
      marginLeft: 44,
      marginBottom: 10,
    },
    couponDescriptionText: {
      maxWidth: 300,
      marginTop: 5,
    },
    couponHeadingText: {
      fontWeight: 'bold',
    },
    plansTrialMessageRow: {
      marginLeft: 44,
      marginTop: 20,
      marginBottom: 30,
    },
    couponRow: {
      flex: 1,
    },
    couponButtonText: {
      fontWeight: 'bold',
      color: '#272929',
    },
});
