/*
  ProfileScreen
*/

import React, { Component } from 'react';
import styles from "./styles";
import Profile from '../../components/Profile';

import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

export default class ProfileScreen extends Component {
  render() {
    return (
      <Profile />
    );
  }
}
