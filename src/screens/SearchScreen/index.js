/*
  Lesson
*/

import React, { Component } from 'react';
import styles from "./styles";
import SearchResultsList from '../../components/Search/SearchResultsList';

import {
  StyleSheet,
} from 'react-native';

import {
  Content,
} from 'native-base';

export default class Lesson extends Component {
  render() {
    return (
      <Content>
        <SearchResultsList />
      </Content>
    );
  }
}
