/*
  TopicScreen
*/

import React, { Component } from 'react';
import styles from "./styles";
import TopicResourceRow from '../../components/Topic/TopicResourceRow';
import TopicLessonList from '../../components/Topic/TopicLesson/TopicLessonList';
import TopicMeterRow from '../../components/Topic/TopicMeterRow';

import {
  StyleSheet,
} from 'react-native';

import {
  Content,
  Text,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
} from 'native-base';

export default class TopicScreen extends Component {
  render() {
    return (
      <Content>
        <TopicMeterRow />
        <TopicResourceRow />
        <TopicLessonList />
      </Content>
    );
  }
}
