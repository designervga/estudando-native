import React from 'react';
import { Text } from 'react-native';

import { storiesOf, addDecorator } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import AppHome from '../../index.android';
import AppBody from '../../src/components/AppBody';
import DisciplinesCollapsible from '../../src/components/DisciplinesCollapsible';
import AppHeader from '../../src/components/AppHeader';
import AppFooter from '../../src/components/AppFooter';
import DisciplinesCollapsibleTopicsList from '../../src/components/DisciplinesCollapsible/DisciplinesCollapsibleTopicsList';
import TopicLessonListItem from '../../src/components/Topic/TopicLesson/TopicLessonListItem';
import TopicResourceEmptyStateRow from '../../src/components/Topic/TopicResourceRow/TopicResourceEmptyStateRow';
import SearchEmptyState from '../../src/components/Search/SearchEmptyState';
import PricingPlan from '../../src/components/PricingPlan';

import TopicScreen from '../../src/screens/TopicScreen';
import SearchScreen from '../../src/screens/SearchScreen';
import ProfileScreen from '../../src/screens/ProfileScreen';
import EditProfileScreen from '../../src/screens/EditProfileScreen';
import DisciplinesScreen from '../../src/screens/DisciplinesScreen';
import LessonScreen from '../../src/screens/LessonScreen';
import OnboardingScreen from '../../src/screens/OnboardingScreen';
import LoginScreen from '../../src/screens/LoginScreen';
import SignUpScreen from '../../src/screens/SignUpScreen';
import RememberPasswordScreen from '../../src/screens/RememberPasswordScreen';
import PlansScreen from '../../src/screens/PlansScreen';
import CheckoutPaymentScreen from '../../src/screens/CheckoutPaymentScreen';
import CheckoutBoletoThankYouScreen from '../../src/screens/CheckoutBoletoThankYouScreen';
import CheckoutPaymentThankYouScreen from '../../src/screens/CheckoutPaymentThankYouScreen';

import {
  StyleProvider,
  Content,
  List,
} from 'native-base';
import getTheme from '../../src/theme/components';
import platform from '../../src/theme/variables/platform';

/* Wrap app theme to all stories */

addDecorator(story => (
  <StyleProvider style={getTheme(platform)}>
    {story()}
  </StyleProvider>
));

/* Stories as components - We need to copy them in a story folder in order to always keep a hardcoded version of them. */

storiesOf('AppBody', module)
  .add('with text', () => (
    <AppBody />
  ));

storiesOf('AppFooter', module)
  .add('with text', () => (
    <AppFooter />
  ));

storiesOf('AppHeader', module)
  .add('with text', () => (
    <AppHeader />
  ));

storiesOf('AppHome', module)
  .add('App Home', () => (
    <AppHome /> /* This should import HomeScreen after coding */
  ));

storiesOf('DisciplinesCollapsible', module)
  .add('Disciplines Collapsible', () => (
    <Content>
      <DisciplinesCollapsible />
    </Content> /* Content mustn't be imported in the production app */
  ))
  .add('Topic List', () => (
    <DisciplinesCollapsibleTopicsList />
  ));

storiesOf('Search', module)
  .add('Search Results Screen', () => (
    <SearchScreen />
  ))
  .add('Search Empty State', () => (
    <SearchEmptyState />
  ));

storiesOf('Topic', module)
  .add('Topic Screen', () => (
    <TopicScreen />
  ))
  .add('Lesson list item', () => (
    <List>
      <TopicLessonListItem />
    </List> /* List mustn't be imported in the production app */
  ))
  .add('Resource Empty State', () => (
      <TopicResourceEmptyStateRow />
  ));

storiesOf('Profile', module)
  .add('Profile Screen', () => (
    <ProfileScreen />
  ))
  .add('Edit Profile Screen', () => (
    <EditProfileScreen />
  ));

storiesOf('Disciplines', module)
  .add('Disciplines Screen', () => (
    <DisciplinesScreen />
  ));

storiesOf('Lesson', module)
  .add('Lesson Screen', () => (
    <LessonScreen />
  ));

storiesOf('Onboarding', module)
  .add('Onboarding Screen', () => (
    <OnboardingScreen />
  ));

storiesOf('Login', module)
  .add('Login Screen', () => (
    <LoginScreen />
  ));

storiesOf('Sign Up', module)
  .add('Sign Up Screen', () => (
    <SignUpScreen />
  ));

storiesOf('RememberPassword', module)
  .add('RememberPassword Screen', () => (
    <RememberPasswordScreen />
  ));

storiesOf('Plans', module)
  .add('Plans Screen', () => (
    <PlansScreen />
  ))
  .add('Plan', () => (
    <PricingPlan />
  ));

storiesOf('Checkout', module)
  .add('CheckoutPayment Screen', () => (
    <CheckoutPaymentScreen />
  ))
  .add('Checkout Boleto Thank You Screen', () => (
      <CheckoutBoletoThankYouScreen />
  ))
  .add('Checkout Thank You Screen', () => (
    <CheckoutPaymentThankYouScreen />
  ));
